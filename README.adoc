= projet_cassandra_python

== prérequis

_The project uses Python 3.8+._

_Server cassandra en local sur port 9042_

_Télécharger Pycharm._

== #A Utiliser si besoin#

Windows users, if needed (if Python can't find packages):

[source,shell script]
----
pip uninstall virtualenv
pip uninstall pipenv
pip install pipenv

----

How it is built:

[source,shell script]
----
pipenv install cassandra-driver
pipenv install colorama
----

To run the program:

[source,shell script]
----
cd python
pipenv run python cassandra-pw.py
----