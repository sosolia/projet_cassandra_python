import sys
# Execution profiles aim at making it easier to execute requests in different ways within a single connected Session.
# Consistency level determines how many nodes in the replica must respond for the coordinator node to successfully
# process a non-lightweight transaction.
# ResultSet : An iterator over the rows from a query result. Materialize the entire result set.
from cassandra.cluster import (Cluster, ResultSet, ExecutionProfile, ConsistencyLevel,
                               EXEC_PROFILE_DEFAULT)
# Load balancing and Failure Handling Policies
from cassandra.policies import (WhiteListRoundRobinPolicy, DowngradingConsistencyRetryPolicy)
# DowngradingConsistencyRetryPolicy deprecated
# Prepared Statements, Batch Statements, Tracing, and Row Factories
from cassandra.query import tuple_factory
# is used to produce colored terminal text and cursor positioning
from colorama import init, deinit, Fore

# is used to init the console output windows
init()

print(Fore.RESET + "\n")
print(Fore.RESET + "==========================================================")
print(Fore.RESET + "----------------------------------------------------------")
print(Fore.RESET + "\n * Hello, let's try to connect to a Cassandra Instance * \n")
print(Fore.RESET + "----------------------------------------------------------")
print(Fore.RESET + "===========================================================")
print(Fore.RESET + "\n")

Errors = 0
verify = True

try:
    profile = ExecutionProfile(
        load_balancing_policy=WhiteListRoundRobinPolicy(['127.0.0.1']),
        retry_policy=DowngradingConsistencyRetryPolicy(),
        consistency_level=ConsistencyLevel.LOCAL_QUORUM,
        serial_consistency_level=ConsistencyLevel.LOCAL_SERIAL,
        request_timeout=15,
        row_factory=tuple_factory
    )

    # Connect to an instance of cassandra.
    cluster = Cluster(execution_profiles={EXEC_PROFILE_DEFAULT: profile})
    session = cluster.connect()

    # Create a keyspace if she not exist.
    print(Fore.RESET + "\n * Trying to create a Keyspace... * \n")
    requestQuery = session.execute(
        """
        CREATE keyspace IF NOT EXISTS xx_demo WITH REPLICATION = {
            'class': 'SimpleStrategy',
            'replication_factor': 1
        }
        """
    )

    # Check for errors when creating a keyspace.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Keyspace successfully create !\n")
        verify = True
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !\n")

    # Create a table if she not exists.
    print(Fore.RESET + "\n * Trying to create a Table... * ")
    requestQuery = session.execute(
        """
            CREATE table IF NOT EXISTS xx_demo.xx_cfdemotable
                    (id uuid, name text, value int, maps map<int, text>, primary key((id), name, value)) 
                    with clustering order by (name asc)
        """
    )

    # Check the create table for errors.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Table successfully create or already exist !\n")
        verify = True
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    # Truncate a table if she is not empty.
    print(Fore.RESET + "\n * Trying to truncate Table if she is not empty * ")
    requestQuery = session.execute(
        """
            TRUNCATE TABLE xx_demo.xx_cfdemoTable
        """
    )

    # Verify errors on the truncate table.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Table successfully truncate !")
        verify = True
    # elif requestQuery == "":
    # verify = False
    # print(Fore.YELLOW + "\n Table already empty !")
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    # Insert some data on a table.
    print(Fore.RESET + "\n * Trying to insert data in Table... * \n")
    requestQuery = session.execute(
        """
            INSERT INTO xx_demo.xx_cfdemoTable(id, name, value, maps) 
                values (uuid(), 'name', 3, {1: 'value1', 2: 'value 2', 3: 'value 3'})
        """
    )

    # Verify errors on the first insert.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Data 1 successfully insert on a table !")
        verify = True
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    requestQuery = session.execute(
        """
            INSERT INTO xx_demo.xx_cfdemoTable(id, name, value, maps) 
                values (uuid(), 'name1', 3, {1: 'value4', 2: 'value 5', 3: 'value 6'})
        """
    )

    # Verify errors on the second insert.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Data 2 successfully insert on a table !")
        verify = True
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    requestQuery = session.execute(
        """
            INSERT INTO xx_demo.xx_cfdemoTable(id, name, value, maps) 
                values (uuid(), 'name2', 3, {1: 'value7', 2: 'value 8', 3: 'value 9'})
        """
    )

    # Verify errors on the third insert.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Data 3 successfully insert on a table !\n")
        verify = True
    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    # Select data From a Table.
    print(Fore.RESET + "\n * Trying to Select data From a Table... * \n")
    requestQuery = session.execute(
        """
            SELECT * 
            FROM xx_demo.xx_cfdemoTable
        """
    )
    # Verify errors on the select.
    if isinstance(requestQuery, ResultSet):
        print(Fore.LIGHTGREEN_EX + "\n Data From xx_cfdemoTable successfully fetched !\n")
        verify = True

    else:
        Errors += 1
        verify = False
        print(Fore.RED + "\n An errors occurred, retry later !")

    # Display the results of our Inserts with a Select
    results = session.execute('SELECT id, name, value, maps FROM xx_demo.xx_cfdemoTable')
    for result in results:
        print(Fore.CYAN + "\n ----------------------------------------")
        print("\n id : ")
        print(result[0])
        print("\n name : ")
        print(result[1])
        print("\n value : ")
        print(result[2])
        print("\n maps : ")
        print(result[3])
        print("\n ----------------------------------------")

    print(Fore.BLUE + "Note : We have the result of three inserts thanks to the Select !")

    # Summary of errors
    if Errors == 0:
        print(Fore.GREEN + "\n All queries were successful !")
        verify = True
    else:
        verify = False
        print(Fore.YELLOW + "\n Number of errors detected : " + Errors)
        print(Fore.Red + "\n Not all queries were successful !")

except:
    print(Fore.RED + "An unexpected error happened !", sys.exc_info()[8])
    raise
# reset the console output to default clear & previous state
deinit()
